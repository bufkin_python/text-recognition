#!/usr/bin/env/Python311

import pytesseract
import cv2

pytesseract.pytesseract.tesseract_cmd = r'C:/Program Files/Tesseract-OCR/tesseract.exe'

image = cv2.imread("./files/1984_image.png")

string = pytesseract.image_to_string(image)
print(string)
